.ONESHELL:
SHELL = bash
.SHELLFLAGS = -ec

bats = ./vendor/bats-core/bin/bats

.PHONY: t, test
t: test	## Shortcut to test
test: ## Run the tests
	$(bats) test